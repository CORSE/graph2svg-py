from typing import Optional, Self, Sequence

import drawsvg
import numpy as np


from .node import Node
from .edge import update_collision_leaf, ArrowMarker, Edge
from .styles import Style, get_default_styles
from .constraint import Constraint


class Sketch(object):
    """The main class to create a svg."""

    def __init__(
        self,
        nodes: Sequence[Node],
        edges: Optional[Sequence[Edge]] = None,
        constraints: Optional[Sequence[Constraint]] = None,
        margin: int = 10,
    ) -> None:
        self.css_styles = get_default_styles()
        self.root = Node(
            nodes, constraints, display=False, margin_x=margin, margin_y=margin
        )
        self.edges: Sequence[Edge] = edges if edges else []

        self.default_arrow_marker = ArrowMarker()

    def set_style(self, style: Style) -> Self:
        """Add or override one default style css class."""
        self.css_styles[style.css_class] = style
        return self

    def set_color_theme(self, theme: str) -> Self:
        for style in self.css_styles.values():
            style.theme = theme
        return self

    def set_default_arrow_marker(self, marker: ArrowMarker) -> Self:
        self.default_arrow_marker = marker
        return self

    def to_svg(self, path: str) -> None:
        self.root.place_children()
        d = drawsvg.Drawing(self.root.width, self.root.height)

        d.append(self.default_arrow_marker.to_svg())
        for style in self.css_styles.values():
            d.append_css(style.to_css())

        self.root.append_to_svg(d)

        collision_map = {}
        window_size = (np.int32(self.root.width), np.int32(self.root.height))
        update_collision_leaf(self.root, collision_map, window_size)
        for edge in self.edges:
            d.append(edge.to_svg(collision_map, window_size))

        d.save_svg(path)
        print(f"Sketch successfuly saved to {path}")
