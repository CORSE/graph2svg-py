# Getting started

This library requires python 3.11 or above.

Check `graph2svg/__main__.py` for an example on how the API is supposed to be used.

The idea of the API is create your some `Node`(s) then connect them using `Edge`(s). Add some constraints to enforce how does the output will look.

** This is a prototype **

# TODOs

- [x] Rectangle with text centered
- [x] Non overlap by default
- [x] Ocaml/Rust enum type like for `Constraint`
- [x] Hierarchical `Node` with ability to nest graphs (constraint are local to the nest only).
- [x] 2D array as a new `Node` (support nesting) which doesn't require solver.
- [x] Circle as a new `Node`
- [x] Self to Self `Edge`
- [x] Label on `Edge`
- [ ] Test some real CFG example
- [ ] Optimisation of the dijkstra for edges by using a step > 1. For example choosing the min of width among nodes for step x and the min of heigth for step y.
- [ ] Improvements (edge's labels can't overlap with nodes or edges)
- [ ] Straight line Edge ?
- [ ] Curve line Edge ?
- [ ] What to do when crossing is unavoidable (can we detect it and change color ?)
