from dataclasses import dataclass
from typing import Optional


from graph2svg.color_schemes import get_theme
from graph2svg.helpers import get_path_to_font

DEFAULT_FONT_SIZE = 12
DEFAULT_OPACITY = 1.0
DEFAULT_FONT = "JetBrainsMonoNerdFont"
DEFAULT_THEME = "catppuccin_latte"
DEFAULT_FONT_TTF = get_path_to_font(DEFAULT_FONT)
DEFAULT_FONT_WEIGHT = "normal"
DEFAULT_FONT_STYLE = "normal"
DEFAULT_RX = 10
DEFAULT_RY = 10
DEFAULT_RECT_NAME = "graph2svg_rect"
DEFAULT_CIRCLE_NAME = "graph2svg_circle"
DEFAULT_TEXT_NAME = "graph2svg_text"
DEFAULT_EDGE_NAME = "graph2svg_edge"
DEFAULT_CELL_NAME = "graph2svg_cell2d"
NON_CSS_ATTRIBUTES = {
    "css_class",
    "ttf_path",
    "theme",
    "rx",
    "ry",
}
COLOR_ATTRIBUTES = {"stroke", "fill", "background"}


@dataclass
class Style(object):
    # name of the class to create in the <style> section
    css_class: str
    theme: str = DEFAULT_THEME

    def to_dict_css(self) -> dict[str, str | int | float]:
        """This function is used for converting python Style class to a css key value pairs."""
        css_dict = {}
        theme_colors = get_theme(self.theme)
        for attribut, value in vars(self).items():
            if (attribut in NON_CSS_ATTRIBUTES) or (value is None):
                continue
            a = attribut.replace("_", "-")
            v = theme_colors.get(value, value) if a in COLOR_ATTRIBUTES else value
            css_dict[a] = v
        return css_dict

    def to_css(self) -> str:
        """Use to_dict to get the correct css attributes and values and generate valid css class to inline."""
        css_core = "\n  ".join(
            (f"{name}: {val};" for name, val in self.to_dict_css().items())
        )
        return f"\n{self.css_class} {{\n  {css_core} \n}}"

    def get_name(self) -> str:
        return self.css_class.replace(".", "")


@dataclass
class StrokeFillStyle(Style):
    # general stroke and fill color style
    stroke: Optional[str] = None
    stroke_width: Optional[int] = None
    stroke_opacity: float = DEFAULT_OPACITY
    fill: Optional[str] = None
    fill_opacity: Optional[float] = DEFAULT_OPACITY


@dataclass
class TextStyle(StrokeFillStyle):
    # text specific style
    font: str = DEFAULT_FONT
    font_size: int = DEFAULT_FONT_SIZE
    font_weight: str = DEFAULT_FONT_WEIGHT
    font_style: str = DEFAULT_FONT_STYLE
    text_anchor: str = "start"
    dominant_baseline: str = "hanging"
    # cached path to file to avoid research again
    ttf_path: str = DEFAULT_FONT_TTF

    def __init__(
        self,
        css_class: str = DEFAULT_TEXT_NAME,
        font: str = DEFAULT_FONT,
        fontsize: int = DEFAULT_FONT_SIZE,
        font_weight: str = DEFAULT_FONT_WEIGHT,
        font_style: str = DEFAULT_FONT_STYLE,
        stroke: str = "text",
        fill: str = "text",
        text_anchor: str = "start",
        dominant_baseline: str = "hanging",
        **kwargs,
    ) -> None:
        """
        Create a text style, you can overwrite the defaults.

        ## Arguments
        - css_class: the name of the generated style css class.
        - theme: the name of the color scheme (should be in AVAILABLE_THEMES) to overwrite some generic color name to their concrete color code
        - stroke_color: string containing color as name like "text" from a color scheme or a raw color code like '#ff00ff'.
        - font: the name of the font (should be exact match with one system font).
        - font_weight: 'normal' | 'bold' | 'bolder' | 'lighter'
        - font_style: 'normal' | 'italique' | 'oblique'
        - other key words arguments are used in StrokeFillStyle parent class
        """
        self.font = font
        self.font_size = fontsize
        self.font_weight = font_weight
        self.font_style = font_style
        self.text_anchor = text_anchor
        self.dominant_baseline = dominant_baseline

        if font != DEFAULT_FONT:
            self.ttf_path = get_path_to_font(font)
        else:
            self.ttf_path = DEFAULT_FONT_TTF
        super().__init__("." + css_class, stroke=stroke, fill=fill, **kwargs)


class RectStyle(StrokeFillStyle):
    # rectangle specific
    # rounded corners
    rx: Optional[int] = None
    ry: Optional[int] = None

    def __init__(
        self,
        css_class: str = DEFAULT_RECT_NAME,
        rounded_x: int = 10,
        rounded_y: int = 10,
        stroke: str = "overlay0",
        fill: str = "surface0",
        **kwargs,
    ) -> None:
        """
        Create a default rect style, you can overwrite the defaults.

        ## Arguments
        - rounded_*: the radius of the circles that will round the corners of the rectangle.
        - other key words arguments are used in StrokeFillStyle parent class
        """
        self.rx, self.ry = rounded_x, rounded_y
        super().__init__("." + css_class, stroke=stroke, fill=fill, **kwargs)


@dataclass
class SvgStyle(Style):
    background: str = "base"

    def __init__(
        self, theme: str = DEFAULT_THEME, background_color: str = "base"
    ) -> None:
        self.background = background_color
        super().__init__("svg", theme)


@dataclass
class EdgeStyle(StrokeFillStyle):
    def __init__(
        self,
        css_class: str = DEFAULT_EDGE_NAME,
        theme: str = DEFAULT_THEME,
        stroke: str = "overlay0",
        stroke_width: int = 1,
        stroke_opacity: float = DEFAULT_OPACITY,
    ) -> None:
        # since this is a custom css class and not a straight overwrite of default we should declare it with the css syntax .myclassname
        super().__init__(
            "." + css_class,
            theme,
            stroke,
            stroke_width,
            stroke_opacity,
            fill="none",
            fill_opacity=None,
        )


@dataclass
class MarkerStyle(StrokeFillStyle):
    def __init__(
        self,
        theme: str = DEFAULT_THEME,
        color: str = "overlay0",
        opacity: float = DEFAULT_OPACITY,
    ) -> None:
        super().__init__(
            "marker",
            theme,
            fill=color,
            fill_opacity=opacity,
        )


@dataclass
class CellStyle(RectStyle):
    def __init__(
        self,
        css_class: str = DEFAULT_CELL_NAME,
        rounded_x: int = 0,
        rounded_y: int = 0,
        stroke: str = "overlay0",
        fill: str = "surface0",
    ) -> None:
        # since this is a custom css class and not a straight overwrite of default we should declare it with the css syntax .myclassname
        super().__init__(
            css_class,
            rounded_x,
            rounded_y,
            stroke,
            fill,
        )


@dataclass
class CircleStyle(StrokeFillStyle):
    def __init__(
        self,
        css_class: str = DEFAULT_CIRCLE_NAME,
        stroke: str = "overlay0",
        stroke_width: Optional[int] = 2,
        stroke_opacity: float = DEFAULT_OPACITY,
        fill: str = "surface0",
        fill_opacity: float = DEFAULT_OPACITY,
    ) -> None:
        # since this is a custom css class and not a straight overwrite of default we should declare it with the css syntax .myclassname
        super().__init__(
            "." + css_class,
            DEFAULT_THEME,
            stroke,
            stroke_width,
            stroke_opacity,
            fill,
            fill_opacity,
        )


def get_default_styles() -> dict[str, Style]:
    default = (
        EdgeStyle(),
        RectStyle(),
        TextStyle(),
        SvgStyle(),
        MarkerStyle(),
        CellStyle(),
        CircleStyle(),
    )
    return {s.css_class: s for s in default}
