from collections.abc import Sequence
from typing import NamedTuple

import numpy as np
from numpy.typing import NDArray


CENTER = 0.5


class AlignHorizontal(NamedTuple):
    nodes_id: Sequence[int]
    anchor: float = CENTER

    def equality(
        self,
        indices: dict[int, int],
        heights: list[int],
        eq_bounds: list[int],
        selected_nodes: list[NDArray],
    ) -> None:
        """
        Same y between all the nodes to align them horizontally.
        # Warning
        this function will mutate eq_bounds and selected_nodes
        """
        nnodes = len(heights)
        ncoords = 2 * nnodes
        for n1, n2 in zip(self.nodes_id, self.nodes_id[1:]):
            i1, i2 = indices[n1], indices[n2]
            h1, h2 = heights[i1], heights[i2]
            selection = np.zeros(ncoords)
            # we expect in the constraints given to scipy all x then all y
            selection[i2 + nnodes] = 1
            selection[i1 + nnodes] = -1

            eq_bounds.append(int(self.anchor * (h1 - h2)))
            selected_nodes.append(selection)


class AlignVertical(NamedTuple):
    nodes_id: Sequence[int]
    anchor: float = CENTER

    def equality(
        self,
        indices: dict[int, int],
        widths: list[int],
        eq_bounds: list[int],
        selected_nodes: list[NDArray],
    ) -> None:
        """
        Same x between all the nodes to align them horizontally.
        # Warning
        this function will mutate eq_bounds and selected_nodes
        """
        ncoords = 2 * len(widths)
        for n1, n2 in zip(self.nodes_id, self.nodes_id[1:]):
            i1, i2 = indices[n1], indices[n2]
            w1, w2 = widths[i1], widths[i2]
            selection = np.zeros(ncoords)
            selection[i2] = 1
            selection[i1] = -1

            eq_bounds.append(int(self.anchor * (w1 - w2)))
            selected_nodes.append(selection)


class LeftToRight(NamedTuple):
    nodes_id: Sequence[int]
    margin_min: int = 10

    def inequality(
        self,
        indices: dict[int, int],
        widths: list[int],
        parent_width: int,
        lb: list[int],
        selected_nodes: list[NDArray],
        ub: list[int],
    ) -> None:
        """
        To choose the order of 2 nodes horizontally we must enforce a constraint on the x axis:
         ```
        2*margin + left.width <= right.x - left.x <= WIDTH - 2*margin - right.width
        ```
        The idea is that we should have the space of the left rect to fit + some margin between the 2 top left corners of the rects. And at max the 2 rects should fit on the window.
        # Warning
        this function will mutate lb, selected_nodes, ub
        """
        ncoords = 2 * len(widths)
        for n1, n2 in zip(self.nodes_id, self.nodes_id[1:]):
            i1, i2 = indices[n1], indices[n2]
            w1, w2 = widths[i1], widths[i2]
            selection = np.zeros(ncoords)
            selection[i2] = 1
            selection[i1] = -1
            lower = 2 * self.margin_min + w1
            upper = parent_width - 2 * self.margin_min - w2
            if upper < lower:
                raise Exception(
                    f"Error: {parent_width=} is too small (or margin={self.margin_min} of LeftToRight is too big)"
                )
            lb.append(lower)
            ub.append(upper)
            selected_nodes.append(selection)


class TopToBottom(NamedTuple):
    nodes_id: Sequence[int]
    margin_min: int = 10

    def inequality(
        self,
        indices: dict[int, int],
        heights: list[int],
        parent_height: int,
        lb: list[int],
        selected_nodes: list[NDArray],
        ub: list[int],
    ) -> None:
        """
        To choose the order of 2 nodes vertically we must enforce a constraint on the y axis:
         ```
        2*margin + top.height <= bottom.y - top.y <= HEIGHT - 2*margin - bottom.height
        ```
        The idea is that we should have the space of the top rect to fit + some margin between the 2 top left corners of the rects. And at max the 2 rects should fit on the window.
        # Warning
        this function will mutate lb, selected_nodes, ub
        """
        nnodes = len(heights)
        ncoords = 2 * nnodes
        for n1, n2 in zip(self.nodes_id, self.nodes_id[1:]):
            i1, i2 = indices[n1], indices[n2]
            w1, w2 = heights[i1], heights[i2]
            selection = np.zeros(ncoords)
            # we expect in the constraints given to scipy all x then all y
            selection[i2 + nnodes] = 1
            selection[i1 + nnodes] = -1

            lower = 2 * self.margin_min + w1
            upper = parent_height - 2 * self.margin_min - w2
            if upper < lower:
                raise Exception(
                    f"Error: {parent_height=} is too small (or margin={self.margin_min} of TopToBottom is too big)"
                )
            lb.append(lower)
            ub.append(upper)
            selected_nodes.append(selection)


Constraint = AlignHorizontal | AlignVertical | LeftToRight | TopToBottom
