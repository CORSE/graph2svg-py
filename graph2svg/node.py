from typing import Any, Optional, Sequence, Self
import drawsvg
import numpy as np
from numpy.typing import NDArray
from scipy.optimize import Bounds, LinearConstraint, NonlinearConstraint, minimize


from .styles import *
from .helpers import Vec2, get_text_size
from .constraint import *


ScipyConstraints = list[LinearConstraint | NonlinearConstraint]


class Node(object):
    """Base class for all node."""

    id_counter: int = 0

    def recursive_max_size(self) -> tuple[int, int]:
        w = self.width + 2 * self.margin_x
        h = self.height + 2 * self.margin_y
        for child in self.children:
            tmp_w, tmp_h = child.recursive_max_size()
            w += tmp_w
            h += tmp_h
        return (w, h)

    def __init__(
        self,
        children: Optional[Sequence["Node"]] = None,
        children_constraints: Optional[Sequence[Constraint]] = None,
        width: Optional[int] = None,
        height: Optional[int] = None,
        margin_x: int = 10,
        margin_y: int = 10,
        style_rect: Optional[RectStyle] = None,
        display: bool = True,
    ) -> None:
        self.id = Node.id_counter
        Node.id_counter += 1
        self.x, self.y = 0, 0
        self.width, self.height = 0, 0
        self.children: Sequence[Self] = children if children else []
        self.constraints: Sequence[Constraint] = (
            children_constraints if children_constraints else []
        )
        # safety check
        ids = {n.id for n in self.children}
        for constr in self.constraints:
            if any((id not in ids for id in constr.nodes_id)):
                raise Exception(
                    "Constraint in a graph should only involve nodes in the graph"
                )
        self.display = display
        self.style_rect = style_rect
        self.margin_x, self.margin_y = margin_x, margin_y
        self.width, self.height = self.recursive_max_size()

        # override the default dimensions
        if width:
            self.width = width
        if height:
            self.height = height
        self.dimensions = np.array((self.width, self.height))

    def get_pos(self) -> Vec2:
        return np.array((self.x, self.y))

    def __hash__(self) -> int:
        return self.id

    def drawing(self) -> drawsvg.DrawingElement:
        style_rect = {"rx": DEFAULT_RX, "ry": DEFAULT_RY, "class": DEFAULT_RECT_NAME}
        if self.style_rect:
            style_rect["rx"] = self.style_rect.rx
            style_rect["ry"] = self.style_rect.ry
            style_rect["class"] = self.style_rect.get_name()
        return drawsvg.Rectangle(self.x, self.y, self.width, self.height, **style_rect)

    def place_children(self):
        if not self.children:
            return

        if len(self.children) > 1:
            constraints = self.constraints_to_scipy()
            positions = self.solve_constraints(constraints)
        else:
            positions = [0, 0]

        n = len(self.children)
        # update child + recursive call
        print(f"origin x = {self.x}, y = {self.y}")
        print(f"width = {self.width}, height = {self.height}")
        for child, x, y in zip(self.children, positions[:n], positions[n:]):
            child.x, child.y = self.x + int(x), self.y + int(y)
            print(f"x = {child.x}, y = {child.y}, w={child.width}, h={child.height}")
            child.place_children()

    def append_to_svg(self, d: drawsvg.Drawing):
        if self.display:
            d.append(self.drawing())

        for child in self.children:
            child.append_to_svg(d)

    def solve_constraints(self, constraints: ScipyConstraints) -> NDArray:
        w, h = self.width, self.height
        # there is a bias to push to the right stuff so I cheat to balance
        # using a max instead of an average
        avg_w = np.max([n.width for n in self.children])
        avg_h = np.average([n.height for n in self.children])
        n = len(self.children)
        n_var = 2 * n
        indices = np.arange(n_var)
        lb = np.where(indices < n, self.margin_x, self.margin_y)
        max_x = [w - c.width - self.margin_x for c in self.children]
        max_y = [h - c.height - self.margin_y for c in self.children]
        ub = np.concatenate((max_x, max_y))
        print("---- x,y bounds ----")
        print(f"{lb=}")
        print(f"{ub=}")
        print("--------------------")
        bounds = Bounds(lb, ub)  # pyright:ignore
        center_x = w // 2 - int(avg_w)
        center_y = h // 2 - int(avg_h)
        center = np.where(np.arange(n_var) < n_var // 2, center_x, center_y)

        def objective_fun(x) -> float:
            """Objective function built with the idea of computing the distance to center and try to minimize the average distance toward the center while maximizing the variance of this distance to the center."""
            manhattan_d = np.abs(x[:n] - center[:n]) + np.abs(x[n:] - center[n:])
            avg = np.average(manhattan_d)
            stddev = np.std(manhattan_d)
            return avg / (1 + stddev)

        print("Solver is running...")
        res = minimize(
            objective_fun,
            center,
            method="trust-constr",
            bounds=bounds,
            constraints=constraints,
        )
        print(f"Solver return success status: {res.success}, {res.message}")
        # assert res.success
        return res.x

    def non_overlap_constraints(self) -> NonlinearConstraint:
        assert self.children
        n_nodes = len(self.children)
        n_coords = 2 * n_nodes
        n_consts = n_nodes * (n_nodes - 1) // 2
        lb = np.full(n_consts, min(self.margin_x, self.margin_y))
        mat = np.zeros((n_consts, n_coords))
        idx = 0
        for i in range(n_nodes):
            node_i = self.children[i]
            dims_i = node_i.dimensions
            for j in range(i):
                node_j = self.children[j]
                dims_j = node_j.dimensions
                # constraint is that distance should be bigger than diag (approximation maybe too bigd)
                lb[idx] += max(dims_i.dot(dims_i), dims_j.dot(dims_j))
                mat[idx, i] = 1
                mat[idx, j] = -1
                # idx on y are the second half
                mat[idx, i + n_nodes] = 1
                mat[idx, j + n_nodes] = -1
                idx += 1

        ub = np.full(n_consts, np.inf)

        def dist(x: NDArray) -> NDArray:
            x_dif = mat[:, 0:n_nodes] @ x[0:n_nodes]
            y_dif = mat[:, n_nodes:] @ x[n_nodes:]
            return np.power(x_dif, 2) + np.power(y_dif, 2)

        print("----- non overlap -----")
        print(f"{lb=}")
        print(f"{ub=}")
        print(f"{mat=}")
        print("-----------------------")

        return NonlinearConstraint(dist, lb, ub)

    def constraints_to_scipy(self) -> ScipyConstraints:
        indices = {n.id: i for i, n in enumerate(self.children)}
        widths = [n.width for n in self.children]
        heights = [n.height for n in self.children]
        eq_bounds, eq_mat = [], []
        ineq_lb, ineq_mat, ineq_ub = [], [], []
        for constr in self.constraints:
            print(constr)
            match constr:
                case AlignHorizontal(_, _):
                    constr.equality(indices, heights, eq_bounds, eq_mat)
                case AlignVertical(_, _):
                    constr.equality(indices, widths, eq_bounds, eq_mat)
                case LeftToRight(_, _):
                    constr.inequality(
                        indices, widths, self.width, ineq_lb, ineq_mat, ineq_ub
                    )
                case TopToBottom(_, _):
                    constr.inequality(
                        indices, heights, self.height, ineq_lb, ineq_mat, ineq_ub
                    )

        res: ScipyConstraints = []
        if eq_bounds:
            print("----- equality -----")
            print(f"{eq_bounds=}")
            print(f"{np.array(eq_mat)}")
            print("-----------------------")
            res.append(
                LinearConstraint(
                    np.array(eq_mat),
                    np.array(eq_bounds),  # pyright:ignore
                    np.array(eq_bounds),  # pyright:ignore
                )
            )
        if ineq_lb:
            print("----- inequality -----")
            print(f"{ineq_lb=}")
            print(f"{ineq_ub=}")
            print(f"{np.array(ineq_mat)}")
            print("-----------------------")
            res.append(
                LinearConstraint(
                    np.array(ineq_mat),
                    np.array(ineq_lb),  # pyright:ignore
                    np.array(ineq_ub),  # pyright:ignore
                )
            )
        res.append(self.non_overlap_constraints())
        return res


def get_font_size(style_text: TextStyle | None) -> int:
    if style_text:
        return style_text.font_size
    else:
        return DEFAULT_FONT_SIZE


def get_ttf(style_text: TextStyle | None) -> str:
    if style_text:
        return style_text.ttf_path
    else:
        return DEFAULT_FONT_TTF


class BoxedText(Node):
    """Basic class for representing item Box filled with text in a graph."""

    def __init__(
        self,
        text: Any,
        margin_x: int = 10,
        margin_y: int = 10,
        inner_margin_x: int = 10,
        inner_margin_y: int = 10,
        style_rect: Optional[RectStyle] = None,
        style_text: Optional[TextStyle] = None,
        display_rect: bool = True,
    ) -> None:
        self.text = str(text)
        self.style_text = style_text
        self.inner_margin_x = inner_margin_x
        self.inner_margin_y = inner_margin_y
        self.display_rect = display_rect

        ttf = get_ttf(style_text)
        fontsize = get_font_size(style_text)
        width, height = get_text_size(self.text, ttf, fontsize)
        width += 2 * inner_margin_x
        height += 2 * inner_margin_y
        super().__init__([], [], width, height, margin_x, margin_y, style_rect)

    def drawing(self) -> drawsvg.DrawingElement:
        css_class = self.style_text.get_name() if self.style_text else DEFAULT_TEXT_NAME
        kwargs = {"class": css_class}

        text_svg = drawsvg.Text(
            self.text,
            get_font_size(self.style_text),
            self.x + self.inner_margin_x,
            self.y + self.inner_margin_y,
            **kwargs,
        )
        if self.display_rect:
            group_svg = drawsvg.Group()
            group_svg.append(super().drawing())

            group_svg.append(text_svg)
            return group_svg
        return text_svg

    def __repr__(self) -> str:
        return f"BoxedText({self.text})"


Array2DCellStyles = dict[tuple[int, int], tuple[CellStyle, TextStyle]]


class Array2D(Node):
    def __init__(
        self,
        content2d: list[list[Any]],
        default_cell_style: Optional[CellStyle] = None,
        default_text_style: Optional[TextStyle] = None,
        override_styles: Optional[Array2DCellStyles] = None,
        margin_x: int = 0,
        margin_y: int = 0,
    ) -> None:
        nrows, ncols = len(content2d), max((len(row) for row in content2d))

        self.array_pos: dict[int, tuple[int, int]] = {}
        override_styles = override_styles if override_styles else {}
        style_cells = default_cell_style if default_cell_style else CellStyle()
        style_text = default_text_style if default_text_style else TextStyle()
        children = []
        for i, row in enumerate(content2d):
            for j, stuff in enumerate(row):
                rect, text = override_styles.get((i, j), (style_cells, style_text))
                if isinstance(stuff, Node):
                    child = stuff
                else:
                    child = BoxedText(
                        stuff, margin_x, margin_y, style_rect=rect, style_text=text
                    )
                children.append(child)
                self.array_pos[child.id] = (i, j)

        self.cell_w = 2 * margin_x + max((c.width for c in children))
        self.cell_h = 2 * margin_y + max((c.height for c in children))

        for c in children:
            c.width = self.cell_w
            c.height = self.cell_h

        width = ncols * self.cell_w
        height = nrows * self.cell_h

        super().__init__(
            children,
            [],
            width,
            height,
            margin_x,
            margin_y,
            display=False,
        )

    def place_children(self):
        # do nothing since children x,y are choosen at creation
        for child in self.children:
            i, j = self.array_pos[child.id]
            child.x = self.x + j * self.cell_w
            child.y = self.y + i * self.cell_h


class Circle(Node):
    def __init__(
        self,
        content: Sequence[Node] | Any,
        children_constraints: Optional[Sequence[Constraint]] = None,
        circle_style: Optional[CircleStyle] = None,
    ) -> None:
        self.circle_style = circle_style
        if isinstance(content, Sequence):
            assert all((isinstance(c, Node) for c in content))
            children = content
        else:
            children: Sequence[Node] = (BoxedText(content, display_rect=False),)
            children_constraints = None

        super().__init__(children, children_constraints, display=True)
        diameter = max(self.width, self.height)
        self.width, self.height = diameter, diameter
        self.radius = diameter // 2

    def drawing(self) -> drawsvg.DrawingElement:
        css_class = (
            self.circle_style.get_name() if self.circle_style else DEFAULT_CIRCLE_NAME
        )
        kwargs = {"class": css_class}
        center_x = self.x + self.radius
        center_y = self.y + self.radius

        return drawsvg.Circle(center_x, center_y, self.radius, **kwargs)
