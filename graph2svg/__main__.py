from graph2svg import *
from graph2svg.node import Array2D, Circle
from graph2svg.styles import CellStyle, TextStyle


def example_nested() -> Sketch:
    salut = BoxedText("Salut")
    bonsoir = BoxedText("Bonsoir")

    nested1 = BoxedText("Nested1")
    nested2 = BoxedText("Nested2")
    meta = Node(
        (nested1, nested2),
        children_constraints=[
            AlignHorizontal((nested1.id, nested2.id)),
            LeftToRight((nested1.id, nested2.id)),
        ],
    )
    return Sketch(
        nodes=(salut, bonsoir, meta),
        edges=[Edge(nested1, nested2)],
        constraints=[
            AlignVertical((salut.id, bonsoir.id)),
            AlignHorizontal((salut.id, meta.id), anchor=0.2),
            LeftToRight((salut.id, meta.id)),
        ],
    )


def example_triangle() -> Sketch:
    coucou = BoxedText("Coucou")
    salut = BoxedText("Salut")
    bonsoir = BoxedText("Bonsoir")

    return Sketch(
        nodes=(coucou, salut, bonsoir),
        edges=[Edge(coucou, salut), Edge(salut, bonsoir), Edge(bonsoir, coucou)],
        constraints=[
            AlignHorizontal((coucou.id, salut.id)),
            AlignVertical((salut.id, bonsoir.id)),
            LeftToRight((coucou.id, salut.id)),
            TopToBottom((bonsoir.id, salut.id)),
        ],
    ).set_color_theme("catppuccin_mocha")


def example_array2d() -> Sketch:
    nrows, ncols = 4, 3
    content = [[i * ncols + j for j in range(ncols)] for i in range(nrows)]
    red_box = CellStyle("myredsquare", fill="red")
    bold_text = TextStyle("myimportanttext", font_weight="bold")
    array = Array2D(
        content,
        override_styles={(nrows // 2, ncols // 2): (red_box, bold_text)},
    )
    return Sketch([array]).set_style(red_box).set_style(bold_text)


def example_array2d_not_full() -> Sketch:
    n = 4
    content = [[i * n + j for j in range(n - i)] for i in range(n)]
    array = Array2D(content)
    return Sketch([array])


def example_circle() -> Sketch:
    coucou = BoxedText("Coucou")
    salut = BoxedText("Salut")
    bonsoir = BoxedText("Bonsoir")
    inner_constr = [
        AlignHorizontal((coucou.id, salut.id)),
        AlignVertical((salut.id, bonsoir.id)),
    ]

    circle = Circle([coucou, salut, bonsoir], inner_constr)
    return Sketch([circle])


def test_edges_pathfinding() -> Sketch:
    coucou = BoxedText("Coucou")
    salut = BoxedText("Salut")
    bonsoir = BoxedText("Bonsoir")
    nuit = BoxedText("Bonne nuit")
    return Sketch(
        (coucou, salut, bonsoir, nuit),
        edges=[
            Edge(coucou, nuit, label="label1", label_up=False),
            Edge(salut, bonsoir, label="label2", label_anchor=0.9, label_up=False),
            Edge(nuit, nuit, label="self to self"),
        ],
        # edges=[Edge(coucou, bonsoir), Edge(salut, nuit)],
        constraints=[
            AlignVertical((coucou.id, salut.id)),
            TopToBottom((coucou.id, salut.id)),
            AlignVertical((bonsoir.id, nuit.id)),
            TopToBottom((bonsoir.id, nuit.id)),
            AlignHorizontal((coucou.id, nuit.id)),
            LeftToRight((coucou.id, nuit.id)),
        ],
    )


def main():
    # sketch = example_triangle()
    # sketch = example_nested()
    # sketch = example_array2d()
    # sketch = example_array2d_not_full()
    # sketch = example_circle()
    sketch = test_edges_pathfinding()
    sketch.to_svg("test.svg")


if __name__ == "__main__":
    main()
