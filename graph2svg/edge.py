from dataclasses import dataclass
from typing import Iterable, NamedTuple, Optional
import heapq

from drawsvg import DrawingElement
import drawsvg
import numpy as np


from .node import Node, get_font_size, get_ttf
from .styles import DEFAULT_TEXT_NAME, DEFAULT_EDGE_NAME, EdgeStyle, TextStyle
from .helpers import Vec2, get_text_size, unit_vec


Coord = tuple[np.int32, np.int32]
CollisionMap = dict[Coord, np.int32]
Priority = tuple[np.int32, np.int32, np.int32, np.int32]

DEFAULT_MARKER_ID = "arrow"
INT_MAX = np.int32(np.iinfo(np.int32).max)
WEIGHT_MAX = np.int32(1000)
INT_MIN = np.int32(0)
EDGE_PENALTY = np.int32(1)
COORD_MIN = (INT_MIN, INT_MIN)
PRIORITY_WORST = (WEIGHT_MAX, INT_MAX, INT_MAX, INT_MAX)

DIR_RIGHT = np.array((1, 0), dtype=np.int32)
DIR_LEFT = np.array((-1, 0), dtype=np.int32)
DIR_DOWN = np.array((0, 1), dtype=np.int32)
DIR_UP = np.array((0, -1), dtype=np.int32)

ROTATE_RIGHT = np.array([[0, -1], [1, 0]], dtype=np.int32)
ROTATE_LEFT = np.array([[0, 1], [-1, 0]], dtype=np.int32)

DIRECTIONS = (DIR_UP, DIR_RIGHT, DIR_DOWN, DIR_LEFT)


def get_centers(n: Node, coef: float = 0.5) -> tuple[Vec2, Vec2, Vec2, Vec2]:
    topleft = n.get_pos()
    left = n.dimensions * DIR_DOWN * coef + topleft
    right = n.dimensions * DIR_DOWN * coef + n.dimensions * DIR_RIGHT + topleft
    up = n.dimensions * DIR_RIGHT * coef + topleft
    down = n.dimensions * DIR_RIGHT * coef + n.dimensions * DIR_DOWN + topleft
    same_order_as_dir = (up, right, down, left)
    return tuple((np.int32(np.round(x)) for x in same_order_as_dir))  # pyright:ignore


def update_collision_edge(points: list[Coord], collision_map: CollisionMap) -> None:
    for (x1, y1), (x2, y2) in zip(points, points[1:]):
        cur_x, cur_y = x1, y2
        dx = np.sign(x2 - x1, dtype=np.int32)
        dy = np.sign(y2 - y1, dtype=np.int32)
        while cur_x != x2 or cur_y != y2:
            key = (cur_x, cur_y)
            value = collision_map.get(key, INT_MIN)
            collision_map[key] = value + EDGE_PENALTY
            cur_x += dx
            cur_y += dy


def update_collision_leaf(
    node: Node,
    collision_map: CollisionMap,
    window_size: Coord,
    margin: int = 15,
    remove_collision=False,
) -> None:
    # Only leafs are avoided when tracing edge
    if not node.children:
        bad_weight = INT_MIN if remove_collision else np.int32(margin)
        x, y = node.x, node.y
        w, h = node.width + 1, node.height + 1
        x_max, y_max = int(window_size[0]), int(window_size[1])
        bad_x = np.arange(
            max(0, x - margin), min(x_max, x + w + margin), dtype=np.int32
        )
        bad_y = np.arange(
            max(0, y - margin), min(y_max, y + h + margin), dtype=np.int32
        )
        bad = (((xi, yi), bad_weight) for xi in bad_x for yi in bad_y)
        collision_map.update(bad)
        illegal_x = np.arange(x + 1, x + w - 1, dtype=np.int32)
        illegal_y = np.arange(y + 1, y + h - 1, dtype=np.int32)
        illegal = (((xi, yi), WEIGHT_MAX) for xi in illegal_x for yi in illegal_y)
        collision_map.update(illegal)
        return
    for child in node.children:
        update_collision_leaf(
            child, collision_map, window_size, margin, remove_collision
        )


class PrioritizedItem(NamedTuple):
    priority: Priority
    point: Vec2
    direction: Vec2

    def __lt__(self, obj):
        return self.priority < obj.priority

    def __gt__(self, obj):
        return self.priority > obj.priority

    def __le__(self, obj):
        return self.priority <= obj.priority

    def __ge__(self, obj):
        return self.priority >= obj.priority


class Path:
    def __init__(self, src: Node, dst: Node) -> None:
        self.src = src
        self.dst = dst
        self.center_dst = dst.get_pos() + (dst.dimensions // 2)
        if self.src == self.dst:
            self.start_points = get_centers(src, 1 / 3)
            self.end_points = get_centers(src, 2 / 3)
        else:
            self.start_points = get_centers(src, 1 / 2)
            self.end_points = get_centers(dst, 1 / 2)

    def dijsktra(
        self,
        weights: CollisionMap,
        window: Vec2,
        starts: Iterable[tuple[Vec2, Vec2]],
        ends: Iterable[Vec2],
    ) -> tuple[PrioritizedItem, dict[Coord, Vec2 | None]]:
        prev: dict[Coord, Vec2 | None] = {}
        priorities: dict[Coord, Priority] = {}

        # initilization of priority queue
        pq: list[PrioritizedItem] = []
        for start, init_dir in starts:
            start_t = tuple(start)
            if (start > 0).all() and (start < window).all():
                priority = (
                    INT_MIN,
                    INT_MIN,
                    INT_MIN,
                    np.sum(np.abs(start - self.center_dst)),
                )
                priorities[start_t] = priority
                prev[start_t] = None
                assert np.count_nonzero(init_dir) == 1
                heapq.heappush(pq, PrioritizedItem(priority, start, init_dir))

        # main loop of A star/dijsktra
        item = None
        end_point = None
        while pq:
            item = heapq.heappop(pq)
            (weight, nturn, dist, _), cur, dir = item

            end_point = next((end for end in ends if (end == cur).all()), None)
            if end_point is not None:
                break
            # iterate through valid neighbors and keep track of number of turns
            dirs_nturns = (
                (dir, nturn),
                (ROTATE_LEFT @ dir, nturn + 1),
                (ROTATE_RIGHT @ dir, nturn + 1),
            )
            for newdir, nturn_nei in dirs_nturns:
                nei = cur + newdir
                nei_t = tuple(nei)
                new_weight: np.int32 = weight + weights.get(nei_t, INT_MIN)
                dist_to_start: np.int32 = dist + 1
                dist_to_end = np.sum(np.abs(nei - self.center_dst))
                new_priority = (new_weight, nturn_nei, dist_to_start, dist_to_end)
                # neighbor should be in the window and get a better dist
                if (
                    (nei > 0).all()
                    and (nei < window).all()
                    and new_priority < priorities.get(nei_t, PRIORITY_WORST)
                ):
                    prev[nei_t] = cur
                    priorities[nei_t] = new_priority
                    heapq.heappush(pq, PrioritizedItem(new_priority, nei, newdir))

        if end_point is None:
            raise Exception(
                f"No path as been found to connect {self.src} to {self.dst}"
            )
        assert item is not None
        return (item, prev)

    def find(
        self,
        weights: CollisionMap,
        coord_max: Coord,
    ) -> list[Coord]:
        """
        Dijsktra algorithm using a custom distance based on number of turn + penalty weight + manhantahn distance to start point and to end point center.

        # Warning
        We expect the weights to have been prepared for this path i.e the calling function remove the margin weights for src and dst and re add them after.
        """
        window = np.array(coord_max)
        print("-" * 20 + "PathFinding" + "-" * 20)
        print(f"start points = {list(map(tuple, self.start_points))}")
        print(f"end points = {list(map(tuple, self.end_points))}")

        min_prev = {}
        min_start = window
        min_item = PrioritizedItem(PRIORITY_WORST, window, window)
        if self.src == self.dst:
            print("Self to Self arc")
            margin = np.int32(15)
            for start, end, dir in zip(self.start_points, self.end_points, DIRECTIONS):
                print(dir)
                assert (start <= end).all()
                if start[0] == end[0]:
                    sign = dir[0]
                    min_x = min(start[0] + sign * margin, start[0])
                    max_x = max(start[0] + sign * margin, start[0])
                    min_y, max_y = start[1] + 1, end[1] - 1
                else:
                    assert start[1] == end[1]
                    sign = dir[1]
                    min_x, max_x = start[0] + 1, end[0] - 1
                    min_y = min(start[1] + sign * margin, start[1])
                    max_y = max(start[1] + sign * margin, start[1])

                range_x = np.arange(min_x, max_x + 1, dtype=np.int32)
                range_y = np.arange(min_y, max_y + 1, dtype=np.int32)
                weights.update((((x, y), margin) for x in range_x for y in range_y))
                item, cur_prev = self.dijsktra(weights, window, [(start, dir)], [end])
                if item.priority < min_item.priority:
                    min_start = start
                    min_item = item
                    min_prev = cur_prev

            end_point = min_item.point
            prev = min_prev
            start = (min_start,)

        else:
            (_, end_point, _), prev = self.dijsktra(
                weights, window, zip(self.start_points, DIRECTIONS), self.end_points
            )
            start = self.start_points

        return self.compact_points(prev, end_point, start)

    def compact_points(
        self, prev: dict[Coord, Optional[Vec2]], end: Vec2, starts: Iterable[Vec2]
    ) -> list[Coord]:
        # get the path with all points with step 1 between each element
        compacted = [(end[0], end[1])]
        p = prev[(end[0], end[1])]
        if p is None:
            raise Exception(f"Nodes {self.src} and {self.dst} are overlapping!")
        old_dir = end - p
        start_point = None
        cur = end
        while start_point is None:
            assert cur is not None
            p = prev[(cur[0], cur[1])]
            assert p is not None
            dir = cur - p
            if (dir.dot(old_dir) == 0).all():
                old_dir = dir
                compacted.append((cur[0], cur[1]))
            cur = p
            start_point = next(
                (start for start in starts if (start == cur).all()),
                None,
            )

        compacted.append((start_point[0], start_point[1]))
        compacted.reverse()
        print(f"{compacted=}")
        return compacted


@dataclass
class ArrowMarker:
    id: str = DEFAULT_MARKER_ID
    width: int = 6
    height: int = 6
    orient: str = "auto-start-reverse"

    def to_svg(self) -> drawsvg.Marker:
        # symmetrical triangle with respect to y=self.height/2
        x0, y0 = 0, 0
        x1, y1 = self.width, self.height // 2  # tip of the arrow
        x2, y2 = 0, self.height
        marker = drawsvg.Marker(
            0,
            0,
            self.width,
            self.height,
            orient=self.orient,
            id=self.id,
            # translation for the marker to fit nicely on the line
            refX=x1 - 1,
            refY=y1,
        )

        path = drawsvg.Path(f"M {x0} {y0} L {x1} {y1} L {x2} {y2} z")
        marker.append(path)
        return marker


@dataclass
class Edge:
    """Connection (that can be bidirectionnal) from one Node to another."""

    from_node: Node
    to_node: Node
    bidirectionnal: bool = False
    label: Optional[str] = None
    label_anchor: float = 0.5
    label_up: bool = True
    style: Optional[EdgeStyle] = None
    style_text: Optional[TextStyle] = None
    marker: Optional[ArrowMarker] = None

    def to_svg(self, weights: CollisionMap, window_size: Coord) -> DrawingElement:
        kwargs = {"class": self.style.get_name() if self.style else DEFAULT_EDGE_NAME}

        if not self.bidirectionnal:
            mark = self.marker.id if self.marker else DEFAULT_MARKER_ID
            kwargs["marker_end"] = f"url(#{mark})"

        path = Path(self.from_node, self.to_node)
        update_collision_leaf(self.to_node, weights, window_size, remove_collision=True)
        points = path.find(weights, window_size)
        assert len(points) >= 2
        update_collision_leaf(self.to_node, weights, window_size)
        update_collision_edge(points, weights)
        flattened = [x_or_y for xy in points for x_or_y in xy]
        lines = drawsvg.Lines(*flattened, **kwargs)
        group = drawsvg.Group()

        if self.marker:
            group.append(self.marker.to_svg())

        if self.label:
            margin = 5
            fontsize = get_font_size(self.style_text)
            ttf = get_ttf(self.style_text)
            text_dims = np.array(get_text_size(self.label, ttf, fontsize))
            css_class = (
                self.style_text.get_name() if self.style_text else DEFAULT_TEXT_NAME
            )
            med_i = int(len(points) * self.label_anchor)
            p1 = np.array(points[max(0, med_i - 1)])
            p2 = np.array(points[min(len(points) - 1, med_i)])
            seg_dims = np.abs(p2 - p1)
            dir = unit_vec(p2 - p1)
            rotate = ROTATE_LEFT if self.label_up else ROTATE_RIGHT
            dir90 = rotate @ unit_vec(p2 - p1)
            center_label_dir = dir * (seg_dims - text_dims) // 2
            margin_label_90 = dir90 * margin
            if self.label_up:
                margin_label_90 += dir90 * text_dims
            label_pos = p1 + center_label_dir + margin_label_90
            xt, yt = int(label_pos[0]), int(label_pos[1])
            print(f"{xt=} {yt=} {p1=} {seg_dims=} {text_dims=}")
            text_kwargs = {"class": css_class}
            text = drawsvg.Text(self.label, fontsize, xt, yt, **text_kwargs)
            group.append(text)
        group.append(lines)
        return group
