from typing import Annotated, Literal

from PIL import ImageFont
import fontconfig
import numpy as np
from numpy.typing import NDArray

Vec2 = Annotated[NDArray[np.int32], Literal[2]]
IntArray2D = Annotated[NDArray[np.int32], Literal["N", "M"]]


def get_path_to_font(family: str) -> str:
    """Use fontconfig to find the path to the ttf file installed on the host system."""
    fonts = fontconfig.query(f":family={family}", select=["file"])  # pyright:ignore
    if fonts:
        return fonts[0]["file"][0]
    else:
        raise Exception(f"font: {family} not found!")


def get_text_size(text: str, ttf_path: str, font_size: int) -> tuple[int, int]:
    """Simple function to get the size of the rectangle used to draw the text using a given font."""
    imfont = ImageFont.truetype(ttf_path, font_size)
    _, _, width, height = imfont.getbbox(text)
    return (width, height)


def unit_vec(vec: Vec2) -> Vec2:
    x = vec[0] / np.abs(vec[0]) if vec[0] != 0 else 0
    y = vec[1] / np.abs(vec[1]) if vec[1] != 0 else 0
    return np.array((x, y))


def unit_vec_abs(vec: Vec2) -> Vec2:
    x = 1 if vec[0] != 0 else 0
    y = 1 if vec[1] != 0 else 0
    return np.array((x, y))
